package pl.codementors.zoo;

import java.io.Serializable;

public class Wolf extends Mammal implements Carnivorous, Serializable{

    public Wolf(){}

    public Wolf(String name, int age, String furColor){
        super(name, age, furColor);
    }

    public Wolf(String name){
        super(name);
    }

    public void howl(){
        System.out.println("Wolf makes howl!!!");
    }

    @Override
    public void eat(){
        System.out.println("Wolf eats meat.");
    }

    @Override
    public void print(){
        System.out.println("Wolf, Name: "+getName());
    }

    @Override
    public void eatMeat(){
        System.out.println("Wolf eat meat");
    }
}
