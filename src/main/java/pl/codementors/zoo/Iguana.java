package pl.codementors.zoo;

import java.io.Serializable;

public class Iguana extends Lizard implements Herbivorous, Serializable{

    public Iguana(){}

    public Iguana(String name, int age, String scaleColor){
        super(name, age, scaleColor);
    }

    public Iguana(String name){
        super(name);
    }

    public void hiss(){
        System.out.println("Iguana makes hiss!!!");
    }

    @Override
    public void eat(){
        System.out.println("Iguana eats plants");
    }

    @Override
    public void print(){
        System.out.println("Iguana, Name: "+getName());
    }

    @Override
    public void eatPlant(){
        System.out.println("Iguana eat plants");
    }
}
