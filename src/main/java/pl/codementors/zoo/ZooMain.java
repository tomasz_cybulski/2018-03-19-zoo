package pl.codementors.zoo;


import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Hello to the ZOO
 * @author Tomasz Cybulski
 * @version 1.0
 *
 */
public class ZooMain {


    public static void main(String[] args) {
        System.out.println("Hello to the ZOO!");
        Animal[] animals = new Animal[10];

        animals[0] = new Wolf("Zenek", 3, "grey");
        animals[1] = new Iguana("Ola", 2, "yellow");
        animals[2] = new Parrot("Agnieszka", 4, "red");
        animals[3] = new Wolf("Tomek", 8, "white");
        animals[4] = new Iguana("Janusz", 6, "green");
        animals[5] = new Parrot("Monika", 2,"blue");
        animals[6] = new Wolf("Zdzich", 1, "black");
        animals[7] = new Iguana("Magda", 2, "green");
        animals[8] = new Parrot("Natalia", 8, "yellow");
        animals[9] = new Wolf("Wolferine", 10, "grey");


        //print(animals);
        //feed(animals);
        saveToFile(animals, "/tmp/zoo");
        //howl(animals);
        //hiss(animals);
        //screech(animals);
        //feedWithMeat(animals);
        //feedWithPlant(animals);
        //readFromFile("/tmp/zoo");
        //printColors(animals);
        saveToBinaryFile(animals,"/tmp/zooBin");
        //readFromBinaryFile("/tmp/zooBin");
    }


    public static void print(Animal[] animal) {
        System.out.println("Our animals are:");
        for (Animal i : animal) {
            if (i instanceof Wolf) {
                i.print();
            }
            if (i instanceof Parrot) {
                i.print();
            }
            if (i instanceof Iguana) {
                i.print();
            }


        }

    }

    static void feed(Animal[] aniaml) {
        System.out.println("Animals eat:");
        for (Animal i : aniaml) {
            if (i instanceof Wolf) {
                i.eat();
            }
            if (i instanceof Parrot) {
                i.eat();
            }
            if (i instanceof Iguana) {
                i.eat();
            }
        }
    }

    static void saveToFile(Animal[] animal, String path) {
        File file = new File(path);
        FileWriter fw = null;
        try {
            fw = new FileWriter(file);
            fw.write("Zwierzeta w zoo:\n");
            for (Animal i : animal) {
                if (i instanceof Wolf) {
                    fw.write("Wolf:\n"+i.getName()+"\n");
                }
                if (i instanceof Parrot) {
                    fw.write("Parrot:\n"+i.getName()+"\n");
                }
                if (i instanceof Iguana) {
                    fw.write("Iguana:\n"+i.getName()+"\n");
                }
            }
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void howl(Animal[] animal){
        for(Animal i : animal){
            if(i instanceof Wolf){
               System.out.print(i.getName()+" ");
               ((Wolf)i).howl();
            }
        }
    }

    static void hiss(Animal[] animal){
        for(Animal i : animal){
            if(i instanceof Iguana){
                System.out.print(i.getName()+" ");
                ((Iguana)i).hiss();
            }
        }
    }

    static void screech(Animal[] animal){
        for(Animal i : animal){
            if(i instanceof Parrot){
                System.out.print(i.getName()+" ");
                ((Parrot)i).screech();
            }
        }
    }

    static void feedWithMeat(Animal[] animal){
        for(Animal i : animal){
            if(i instanceof Carnivorous){
                ((Carnivorous)i).eatMeat();
            }
        }
    }

    static void feedWithPlant(Animal[] animal){
        for(Animal i : animal){
            if(i instanceof Herbivorous){
                ((Herbivorous)i).eatPlant();
            }
        }
    }

    static Animal [] readFromFile(String path){
        File file = new File(path);
        Animal [] animalsRead = new Animal[10];
        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)){
            while ((br.readLine()) != null){
                for(int i=0; i<animalsRead.length; i++){
                    String type = br.readLine();
                    switch(type){
                        case"Wolf:":
                            animalsRead[i] = new Wolf(br.readLine());
                            break;
                        case"Parrot:":
                            animalsRead[i] = new Parrot(br.readLine());
                            break;
                        case"Iguana:":
                            animalsRead[i] = new Iguana(br.readLine());
                            break;
                    }
               }
           }

        } catch (IOException ex){
            System.err.println(ex.getMessage());
        }
        System.out.println("Animals read from the file:");
        for(Animal a : animalsRead){
            a.print();
        }
        return animalsRead;
    }

    static void printColors(Animal[] animal){
        for(Animal i : animal){
            if(i instanceof Wolf){
                System.out.println("Name: "+i.getName()+" FurColor: "+ ((Wolf) i).getFurColor());
            }
            if(i instanceof Parrot){
                System.out.println("Name: "+i.getName()+" FeatherColor: "+ ((Parrot) i).getFeatherColor());
            }
            if(i instanceof Iguana){
                System.out.println("Name: "+i.getName()+" ScaleColor: "+ ((Iguana) i).getScaleColor());
            }
        }
    }

    static void saveToBinaryFile(Animal[] animal, String path){
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(path));
            out.writeObject(animal);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static Animal[] readFromBinaryFile(String path){
        Animal[] animal = new Animal[10];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fis);
            animal = (Animal[])in.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return animal;
    }
}
