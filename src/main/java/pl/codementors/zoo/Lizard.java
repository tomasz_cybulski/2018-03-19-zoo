package pl.codementors.zoo;

public abstract class Lizard extends Animal{

    private String scaleColor;

    public Lizard(){
    }

    public Lizard(String name, int age, String scaleCoror){
        super(name, age);
        this.scaleColor = scaleCoror;
    }

    public Lizard(String name){
        super(name);
    }

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
