package pl.codementors.zoo;

import java.io.Serializable;

public class Parrot extends Bird implements Herbivorous, Serializable{

    public Parrot(){}

    public Parrot(String name, int age, String featherColor){
        super(name, age, featherColor);
    }

    public Parrot(String name){
        super(name);
    }

    public void screech(){
        System.out.println("Parrot makes screech!!!");
    }

    @Override
    public void eat(){
        System.out.println("Parrot eats plants");
    }

    @Override
    public void print(){
        System.out.println("Parrot, Name: "+getName());
    }

    @Override
    public void eatPlant(){
        System.out.println("Parrot eat plants");
    }
}
